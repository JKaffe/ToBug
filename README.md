# ToBug.
Todo tracking system. 

## Bugmenu
This script provides a graphical way of using the system, with  dmenu.

## Getting started
You will need to set $BUG_PROJECT as an instance variable indicating where to store the todo tasks. You can add the following to your .bashrc :
```{bash}
export BUG_PROJECT=$HOME/todo
```
Now you will need to create the database.
```{bash}
bug create
```

## License
This project is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; version 2 of the License.

## Acknowledgements
[Lluís Batlle i Rossell](http://vicerveza.homeunix.net/~viric/soft/bug/) wrote the original "bug" script.
